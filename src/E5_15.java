/**
 * @Author John Yun
 * @email  john.yun@sjsu.edu
 * @date 10/31
 *
 * This class demonstrates the usage of the collections interface
 */

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.LinkedHashMap;

public class E5_15 {
    public static void main(String[] args) throws FileNotFoundException {

        // validating arguments length

        if(args.length != 1) {
            System.err.println("Need a command line argument for the file to read.");
            System.exit(1);
        }

        // tokenization
        Map<String, ArrayList<String>> map = new LinkedHashMap<String, ArrayList<String>>();
        Scanner scanner = new Scanner(new File(args[0]));
        while(scanner.hasNextLine()) {
            // scanning each line
            String line = scanner.nextLine();
            Scanner in = new Scanner(line);
            in.useDelimiter("[^A-Za-z0-9_]+");

            while(in.hasNext()) {
                // scanning each token
                String token = in.next();
                // updating the value of each token in the map
                ArrayList<String> lines = map.getOrDefault(token, new ArrayList<String>());
                lines.add(line);
                map.put(token, lines);
            }
            in.close();
        }
        scanner.close();

        // printing
        int index = 0;

        for(String key : map.keySet()) {
            // printing the key
            System.out.println(index + ": " + key + " occurs in:");
            // printing the values
            for(String line : map.get(key))
            {
                System.out.println(line);
            }
            System.out.println();
            ++index;
        }
    }
}